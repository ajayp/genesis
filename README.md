# Description
`genesis` is a utility that helps manage an AWS account from beginning and bootstrapping to end.

# Installation
To install, simply run:

`curl https://gitlab.com/ajayp/genesis/raw/master/install.sh | bash`

Once you have successfully installed `genesis`, running `genesis version` should yield the current installed version.

If you're on macOS, you can get all the tools `genesis` needs to run setup with `sudo -H genesis workspace install`.

### Troubleshooting
_Note: If you see any "Permission Denied." errors, you may need `sudo` access. Install with this command instead:_

`curl https://gitlab.com/ajayp/genesis/raw/master/install.sh | sudo bash && sudo chown -R $(whoami) /usr/local/`

# Usage
To find out what commmands can be run with `genesis` run `genesis help`.

To see what version you are on, run `genesis version`.

To update, run `genesis update`.

In general, using `genesis` you can:

1. Create IAM Users/User Groups with different privileges to access AWS.

2. Register a domain and point it to a hosted zone on Route53.

3. Create a subdomains and create a CNAME record, or point it do a S3 Bucket for Static Site Hosting.

4. Create a VPC in a region with subnets split across available AZs with an Internet Gateway to access the global network. 

5. Create load balancers and auto-scaling groups and optionally map them to CNAME records (domain or sub-domain name) in Route53.

6. Create Key Pairs for SSH access to EC2 instances and optionally install them on the current machine.

7. Create an Amazon ECS ECR Docker registry in a region.

8. Create an Amazon ECS ECR Docker repository on a registry.

9. Create an Amazon S3 Bucket, optionally making it publicly accessible, and optionally adding a CNAME

10. Create an Amazon EFS file system.






