#!/bin/bash

# Change directory to home folder
cd /home/ec2-user

# Log the output of this script to a log file
exec > >(tee /var/log/user-data.log|logger -t user-data ) 2>&1

# Install some dependencies
sudo yum install python docker docker-compose curl nfs-utils  -y

# Run the docker daemon
sudo groupadd docker
sudo gpasswd -a ${USER} docker
sudo service docker restart
newgrp docker

# Log docker in
$(aws ecr get-login --region us-east-1)

# Pull docker images
docker pull nginx:latest

# Run the nginx docker container for healthcheck on port 80
sudo docker run -d -p 80:80 nginx:latest
